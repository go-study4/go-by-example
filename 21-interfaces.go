package main

import "fmt"

func main() {
	r := &rect2{
		width:  1,
		height: 4,
	}

	fmt.Println("area=", r.area())
	fmt.Println("perim=", r.perim())
}

type geometry interface {
	// area 面积
	area() float64
	// perim 周长
	perim() float64
}

type rect2 struct {
	width  float64
	height float64
}

func (r *rect2) area() float64 {
	return r.width * r.height
}

func (r *rect2) perim() float64 {
	return (r.width + r.height) * 2
}
