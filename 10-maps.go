package main

import "fmt"

func main() {
	m := make(map[string]int)

	m["k1"] = 7
	m["k2"] = 13

	v1 := m["k1"]
	fmt.Println("v1: ", v1)
	fmt.Println("len:", len(m))

	delete(m, "k2")
	fmt.Println("map after delete:", m)

	v, prs := m["k2"]
	fmt.Println("prs:", prs)
	// 返回的是零值
	fmt.Println("v:", v)

	n := map[string]int{"foo": 1, "bar": 2}
	fmt.Println("init map :", n)
}
