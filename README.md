# go-by-example



## Getting started

https://gobyexample-cn.github.io/

- [x] [hello-world](01-hello-world.go)
- [x] [值](02-values.go)
- [x] [变量](03-variables.go)
- [x] [常量](04-constants.go)
- [x] [for循环](05-for.go)
- [x] [if/else分支](06-if-else.go)
- [x] [switch分支](07-switch.go)
- [x] [数组](08-arrays.go)
- [x] [切片](09-slices.go)
- [x] [map](10-maps.go)
- [x] [range遍历](11-range.go)
- [x] [函数](12-functions.go)
- [x] [多返回值](13-multiple-return-values.go)
- [x] [变参函数](14-variadic-functions.go)
- [x] [闭包](15-closures.go)
- [x] [递归](16-recursion.go)
- [x] [指针](17-pointers.go)
- [x] [字符串和rune类型](18-strings-and-runes.go)
- [x] [结构体](19-structs.go)
- [x] [方法](20-methods.go)
- [x] [接口](21-interfaces.go)
- [x] [组合](22-embedding.go)
- [x] [泛型](23-generics.go)
- [x] [错误处理](24-errors.go)
- [x] [协程](25-goroutines.go)
- [x] [通道](26-channels.go)
- [x] [通道缓冲](27-channel-buffering.go)
- [x] [通道同步](28-channel-synchronization.go)
- [x] [通道方向](29-channel-directions.go)
- [x] [通道选择器](30-select.go)
- [x] [定时器](35-timers.go)
- [x] [打点器](36-tickers.go)
