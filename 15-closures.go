package main

import "fmt"

// 闭包
func main() {
	nextInt := intSeq()
	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())

	newInts := intSeq()
	fmt.Println(newInts())
}

// intSeq 闭包写法，定义了一个函数，返回值是一个函数
func intSeq() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}
