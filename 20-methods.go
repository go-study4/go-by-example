package main

import "fmt"

// 方法
func main() {
	r := rect{
		width:  10,
		height: 5,
	}
	fmt.Println("area: ", r.area())
	fmt.Println("perim:", r.perim())

	rp := &r
	fmt.Println("area: ", rp.area())
	fmt.Println("perim:", rp.perim())
}

type rect struct {
	width  int
	height int
}

// area 面积
func (r *rect) area() int {
	fmt.Printf("[area]结构体的指针: %p\n", r)
	return r.width * r.height
}

// perim 周长
func (r rect) perim() int {
	fmt.Printf("[perim]结构体的指针: %p\n", &r)
	return (r.height + r.width) * 2
}
