package main

import "fmt"

func main() {
	pings := make(chan string, 1)
	pongs := make(chan string, 1)
	ping(pings, "passed message")
	pong(pings, pongs)
	fmt.Println(<-pongs)
}

// ping 通道只能写入
func ping(pingChan chan<- string, msg string) {
	pingChan <- msg
}

// pong pings 只读，pongs 只写
func pong(pings <-chan string, pongs chan<- string) {
	msg := <-pings
	pongs <- msg
}
