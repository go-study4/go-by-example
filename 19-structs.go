package main

import "fmt"

// 结构体
func main() {
	fmt.Println(newPerson("Jon"))
}

type person struct {
	age  int
	name string
}

func newPerson(name string) *person {
	return &person{
		name: name,
		age:  18,
	}
}
