package main

import "fmt"

// 变参函数
func main() {
	fmt.Println("1 + 2 =", sum(1, 2))
	nums := []int{1, 2, 3}
	fmt.Println("1 + 2 + 3 =", sum(nums...))
}

func sum(nums ...int) int {
	var total int
	for _, num := range nums {
		total = num + total
	}
	return total
}
