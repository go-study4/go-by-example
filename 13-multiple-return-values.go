package main

import "fmt"

// main 多返回值
func main() {
	a, b := vals()
	fmt.Println(a)
	fmt.Println(b)
}

func vals() (int, int) {
	return 1, 2
}
